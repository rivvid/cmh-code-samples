'use strict';

(function ($, window, document, _, undefined) {

//////////////////////////////////////////////////////////////////////////////
// ObservableObject
// observable getter and setter of a hash collection
//////////////////////////////////////////////////////////////////////////////

    window.ACTIVE.powerplant.register('ObservableObject');

    window.ACTIVE.powerplant.ObservableObject = function (options) {

        var params = options.params || {};

        var object = $.extend({},params);
        var broadcastPrefix = options.eventPrefix || 'prefix';
        var broadcastProxy;
        if (!options.broadcaster || (typeof options.broadcaster === 'undefined')) {
            broadcastProxy = this;
        } else {
            broadcastProxy = options.broadcaster;
        }

        return {
            broadcaster: broadcastProxy,
            getObject: function () {
                return object;
            },
            get: function (key) {
                return object[key];
            },
            set: function (key, val) {
                var oldVal = object[key];
                var newVal = val;
                if ($.isArray(object[key])) {
                    oldVal = object[key].slice(0);
                    if (val === null) {
                        object[key] = [];
                    } else {
                        var i = _.indexOf(object[key], val);
                        if (i >= 0) {
                            object[key].splice(i,1);
                        } else {
                            object[key].push(val);
                        }
                        newVal = object[key].slice(0);
                    }
                } else {
                    object[key] = val;
                }
                // trigger key-specific event
                $(broadcastProxy).trigger(broadcastPrefix + key, { key: key, value: newVal, oldValue: oldVal });
                // trigger prefix-only event
                $(broadcastProxy).trigger(broadcastPrefix, { key: key, value: newVal, oldValue: oldVal });
            },
            removeFromArray: function (key, val) {
                var oldVal = object[key];
                if ($.isArray(object[key])) {
                    var i = _.indexOf(object[key], val);
                    if (i >= 0) {
                        object[key].splice(i,1);
                    }
                }
                // trigger key-specific event
                $(broadcastProxy).trigger(broadcastPrefix + key, { key: key, value: object[key], oldValue: oldVal });
                // trigger prefix-only event
                $(broadcastProxy).trigger(broadcastPrefix, { key: key, value: object[key], oldValue: oldVal }); 
            },
            reset: function (newObj) {
                for (var key in object) {
                    // trigger key-specific event
                    $(broadcastProxy).trigger(broadcastPrefix + key, { key: key, value: newObj[key], oldValue: object[key] });
                }
                // trigger prefix-only event
                $(broadcastProxy).trigger(broadcastPrefix,{});
                
                object = $.extend({},newObj);
            }
        };
    }

})(jQuery, window, document, window._);
