'use strict';

//////////////////////////////////////////////////////////////////////////////
// here we go...
//////////////////////////////////////////////////////////////////////////////
(function ($, window, document, undefined) {

    var AddFamilyModal = function (options, element) {
        this.options = options;
        this.element = $(element);
        this.showing = false;
        this.familyAdded = false;
        this.skipJumpToResults = false;
        this._init();
    }

    AddFamilyModal.prototype = {
        _init: function () {
            var self = this;

            this.eventPrefix = 'familymodal:value:changed:';

            // create a scope object
            this.familyMemberObj = window.ACTIVE.powerplant.factory('ObservableObject', {
                params: this._getBlankFamilyMemberObject(),
                eventPrefix: this.eventPrefix
            });
            
            // close modal button
            this.element.find('span.close').bind('click', $.proxy(this, 'hide'));
            
            // initialize switches
            this.element.find('ul[data-bind]').switchGroup({ scope: this.familyMemberObj, eventPrefix: this.eventPrefix });
            
            // load interests
            this.options.ajaxService.getOnce('/interests/kids',function (response) {
                $.each(response.interests, function (i, interest) {
                    self.element.find('ul.switches[data-bind="interests"]').switchGroup('addElement','<li data-value="' + interest.id + '">' + interest.name + '</li>');
                });
            });

            // age input - also allow only number input
            this.element.find('#family-member-age').bind('keyup', function (e) {
                var input = $(this);
                setTimeout(function () {
                    self.familyMemberObj.set('age',input.val());
                },0);                
                self.element.find('.slide1 .step2').removeClass('disabled');
            }).bind('keydown', function (e) {
                // Allow: backspace, delete, tab, escape, enter and .
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
                     // Allow: Ctrl+A
                    (e.keyCode == 65 && e.ctrlKey === true) || 
                     // Allow: home, end, left, right
                    (e.keyCode >= 35 && e.keyCode <= 39)) {
                         // let it happen, don't do anything
                         return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });

            // listen for a gender selection
            $(this.familyMemberObj.broadcaster).bind(this.eventPrefix + 'gender', function () {
                self.element.find('.slide1 .step3').removeClass('disabled');
            });

            // listen for an interest selection
            $(this.familyMemberObj.broadcaster).bind(this.eventPrefix + 'interests', function (e, data) {
                if (data.value.length === 0) { // dont do anything if there aren't any interests selected
                    return false;
                }
                // the button will be visible on desktop, not on mobile
                if (self.element.find('button.continue:visible').length >= 1) {
                    self.element.find('button.continue').attr('disabled',false);
                    // show the slide 1 arrow graphic
                    self.element.find('.slide1 img.arrow').fadeIn('fast');
                } else {
                    // for mobile, skip clicking the button
                    self._goStep2(true);
                }                
            });

            // continue button
            this.element.find('button.continue').attr('disabled',true).on('click', function(){
                self._goStep2();
            });
            
            // name input
            this.element.find('.js-nickname-input').bind('keyup', function (e) {
                var input = $(this);
                setTimeout(function () {
                    self.familyMemberObj.set('name',input.val());
                    self.element.find('#ribbon-preview').html(input.val().substr(0,1).toUpperCase());
                },0);                
                self.element.find('.js-cp-ribbon').removeClass('disabled');
                self.element.find('.js-body-content').removeClass('hidden');
                self.element.find('.js-body-content').removeClass('disabled');
                self.element.find('#btn-save-family-member').attr('disabled',false);
                self.element.find('#btn-add-another-family-member').attr('disabled',false);
            });

            // color picker
            this.element.find('.colorpicker ul li').bind('click', $.proxy(this, '_colorSelected'));

            // save button
            this.element.find('#btn-save-family-member').bind('click', $.proxy(this, '_save'));

            // add another button
            this.element.find('#btn-add-another-family-member').bind('click', function () {
                self._save(true);
                self._reset(true);
            });

        },
        _getBlankFamilyMemberObject: function () {
            return {
                age: '',
                gender: '',
                interests: [],
                name: '',
                color: '6ACEF0' // default blue color
            }
        },
        _save: function (resetAfterSave) {
            var obj = this.familyMemberObj.getObject();
            this.options.familyService.addFamilyMember(obj);
            this.familyAdded = true;
            if (resetAfterSave === true) {
                this._reset();
            } else {
                // close the modal
                this.hide();
            }            
        },
        _reset: function (forceScopeReset) {
            if (forceScopeReset) {
                this.familyMemberObj.reset(this._getBlankFamilyMemberObject());
            }
            // age input
            this.element.find('#family-member-age').val('').focus();
            // dim slide 1 steps 2 and 3
            this.element.find('.slide1 .step2, .slide1 .step3').addClass('disabled');
            // hide the slide 1 arrow graphic
            this.element.find('.slide1 img.arrow').hide();
            // name input
            this.element.find('.js-nickname-input').val('');
            // continue button
            this.element.find('button.continue').attr('disabled',true);
            // slide2 ui elements
            this.element.find('.js-cp-ribbon').addClass('disabled');
            this.element.find('.js-body-content').addClass('hidden');
            this.element.find('.js-body-content').addClass('disabled');
            // color picker
            this.element.find('.colorpicker ul li.current').removeClass('current');
            this.element.find('.colorpicker ul li:first').addClass('current');
            this.element.find('#ribbon-preview').css('backgroundColor','#' + this.element.find('.colorpicker ul li:first').data('value'));
            // save button
            this.element.find('#btn-save-family-member').attr('disabled',true);
            // add another button
            this.element.find('#btn-add-another-family-member').attr('disabled',true);
            //
            this._goStep1();
        },
        _goStep1: function () {
            this.element.find('.slide1').removeClass('disabled');
            this.element.find('.slide2').addClass('disabled');
        },
        _goStep2: function (skipFocus) {
            if (typeof skipFocus === 'undefined') {
                skipFocus = false;
            }
            this.element.find('.slide1').addClass('disabled');
            this.element.find('.slide2').removeClass('disabled');
            if (!skipFocus) {
                var self = this;
                setTimeout(function () {
                    self.element.find('.js-nickname-input').focus();
                }, 1000);
            }
        },
        _colorSelected: function (e) {
            this.element.find('.colorpicker ul li.current').removeClass('current');
            var hex = $(e.currentTarget).data('value');
            $(e.currentTarget).addClass('current');
            this.element.find('#ribbon-preview').css('backgroundColor','#'+hex);
            this.familyMemberObj.set('color',hex);
        },
        show: function (skipJumpToResults) {
            if (typeof skipJumpToResults === 'undefined') {
                skipJumpToResults = false;
            }
            this.skipJumpToResults = skipJumpToResults;
            this.familyAdded = false;
            this._reset(true);
            this.element.fadeIn('fast');
        },
        hide: function () {
            this.element.fadeOut('fast');
            if ((this.familyAdded === true) && (this.skipJumpToResults === false)) {
                window.location.href = '#refine-search';
            }
        }
        
    }
    $.widget.bridge('familyModal', AddFamilyModal);

})(jQuery, window, document);