'use strict';

(function ($, window, document, undefined) {

//////////////////////////////////////////////////////////////////////////////
// $.fn.boundInput()
// jQuery plugin/widget
// bound form input - binds input data to scope data object
//////////////////////////////////////////////////////////////////////////////

    var BoundInput = function (options, element) {
        this.options = options;
        this.element = $(element);
        //
        this.bind = this.element.data('bind');
        this.nulls = this.element.data('nulls');
        if (this.nulls) {
            this.nulls = this.nulls.split(',');
        }

        this.element.bind('change', $.proxy(this, '_handleInput'));

        $(this.options.scope.broadcaster).on(this.options.eventPrefix + this.bind, $.proxy(this, '_handleScopeChange'));
    };

    BoundInput.prototype = {
        _handleInput: function () {
            this.options.scope.set(this.bind, this.element.val());
            if (this.nulls) {
                for (var i=0, len=this.nulls.length; i<len; i++) {
                    this.options.scope.set(this.nulls[i],null);
                }
            }
        },
        _handleScopeChange: function (e, data) {
            this.element.val(data.value);
        }
    }; // BoundInput.prototype

    $.widget.bridge('boundInput', BoundInput);

})(jQuery, window, document);