'use strict';

(function ($, window, document, undefined) {

    
//////////////////////////////////////////////////////////////////////////////
// $.fn.searchFilterBar()
// jQuery plugin/widget
// kids search filter bar - the overall wrapper to manage tabs etc.
//////////////////////////////////////////////////////////////////////////////

    var SearchFilterBar = function ( options, element ) {
        this.options = options;
        this.element = $(element);
        this._init();
    };

    SearchFilterBar.prototype = {
        _init: function () {
            var self = this;
            this.element.find('> ul li').bind('click', function () {
                if ($(this).hasClass('active')) {
                    self.closeSearchFilter();
                } else {
                    self.openSearchFilter($(this).data('filter'));
                }
            });
            this.element.find('footer a.reset-filters').bind('click', function (e) {
                self.options.scope.reset();
                // reset all family members to selected
                self.options.familyService
                    .getFamilyMembers()
                    .then(function (fam) {
                        $.each(fam, function (i, f) {
                            self.options.scope.removeFromArray('who',f.id);
                            self.options.scope.set('who',f.id);
                        });
                    });
                e.preventDefault();
            });

            // external event broadcaster
            if (typeof this.options.broadcaster === 'undefined') {
                this.options.broadcaster = {};
            }

            var eligibleMessage = 'Show only events that offer Active Advantage member discounts';
            // active eligible help tooltip
            this.element.find('span.help').popover({
                placement: 'right',
                trigger: 'manual',
                template: '<div class="popover"><div class="arrow"></div><div class="popover-inner"><div class="popover-content"><p></p></div></div></div>',
                content: function () {
                    return '<div class="advantage-popup-container"><div id="advantage-popup-text">' + eligibleMessage + '</div></div>';
                }
            })
            .on('mouseenter', function () {
                $(this).popover('show');                    
            })
            .on('mouseleave', function (event) {
                $(this).popover('hide');
            })
            .on('click', function (event) {
                if(window.outerWidth < 768){
                    alert(eligibleMessage);
                }
            });

            // initialize switches
            this.element.find('ul[data-bind]').switchGroup({ scope: self.options.scope, eventPrefix: 'filter:value:changed:' });
            this.element.find('input[data-bind]').boundInput({ scope: self.options.scope, eventPrefix: 'filter:value:changed:' });

            // load interests
            this.options.ajaxService.getOnce('/interests/kids',function (response) {
                $.each(response.interests, function (i, interest) {
                    self.element.find('ul.switches[data-bind="what"]').switchGroup('addElement','<li data-value="' + interest.id + '">' + interest.name + '</li>');
                });
            });

            function getChildSwitchHtml (data_obj) {
                return '<li data-value="' + data_obj.id + '"><span class="name">' + data_obj.name + '</span><span class="controls"><div class="ribbon-initial" style="background-color: #' + data_obj.color + ';">' + data_obj.name.substr(0,1).toUpperCase() + '</div><span aria-hidden="true" class="edit-icon icon-uniL600" title="Edit Details for '+ data_obj.name +'"></span></span></li>';
            }
            function showFamilyMembers () {
                $('.add-family-message').hide();
                $('.kids-added').show();
                // hide the add link if there are already 10 family members
                if (self.element.find('ul.switches[data-bind="who"] li').length >= 10) {
                    self.element.find('.add-family-member-link').hide();
                }
            }
            function hideFamilyMembers () {
                $('.add-family-message').show();
                $('.kids-added').hide();
            }

            // setup family member bindings and functions
            function renderFamilyMembers (fam) {
                if (fam && fam.length) {                    
                    $.each(fam, function (i, f) {
                        self.options.scope.set('who',f.id);
                        self.element.find('ul.switches[data-bind="who"]').switchGroup('addElement',getChildSwitchHtml(f));
                    });
                    showFamilyMembers();                 
                }
            }
            function loadAllFamilyMembers () {
                self.options.familyService
                    .getFamilyMembers()
                    .then(renderFamilyMembers);
            }
            $(this.options.familyService.broadcaster)
                .on('family:added', function (event, newMember) {
                    self.options.scope.set('who',newMember.id);
                    self.element.find('ul.switches[data-bind="who"]').switchGroup('addElement',getChildSwitchHtml(newMember));
                    showFamilyMembers();
                    // close the filter details
                    self.closeSearchFilter();
                });
            $(this.options.familyService.broadcaster)
                .on('family:removed', function (event, deletedMember) {
                    self.options.scope.removeFromArray('who',deletedMember.id);
                    self.element.find('ul.switches[data-bind="who"] li[data-value="' + deletedMember.id + '"]').remove();
                    if (self.options.familyService.getTotalFamilyMembers() === 0) {
                        hideFamilyMembers();
                    };
                });

            // load all family members on init
            loadAllFamilyMembers();

            // add family member link
            this.element.find('.add-family-member-link').bind('click', function (e) {
                $('.family-modal').familyModal('show');
            });
            
            // this will prevent clicks in the filter bar from firing the body.click()
            // events we add below in the '_addBodyClickClose()' method
            this.element.bind('click', function (e) {
                e.stopPropagation();
            });
        },
        openSearchFilter: function (which) {
            this.closeSearchFilter();
            this.element.addClass('filter-open');
            this.element.find('ul li[data-filter="'+which+'"]').addClass('active');
            this.element.find('.filter-details-wrapper div.active').removeClass('active');
            this.element.find('.filter-details-wrapper div.' + which).addClass('active');
            this._addBodyClickClose();
            $(this.options.broadcaster).trigger('search-filter-opened');
        },
        closeSearchFilter: function () {
            this.element.find('> ul li.active').removeClass('active');
            this.element.removeClass('filter-open');
            this._removeBodyClickClose();
            $(this.options.broadcaster).trigger('search-filter-closed');
        },
        _bodyClicked: function (e) {
            this.closeSearchFilter();
        },
        _addBodyClickClose: function () {
            $('body').bind('click', $.proxy(this, '_bodyClicked'));
        },
        _removeBodyClickClose: function () {
            $('body').unbind('click', $.proxy(this, '_bodyClicked'));
        }
    };
    $.widget.bridge('searchFilterBar', SearchFilterBar);

})(jQuery, window, document);