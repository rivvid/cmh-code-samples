'use strict';

(function ($, window, document, _, undefined) {

//////////////////////////////////////////////////////////////////////////////
// $.fn.switchGroup()
// jQuery plugin/widget
// switch group - contains multiple switches and binds scope data
//////////////////////////////////////////////////////////////////////////////

    var SwitchGroup = function (options, element) {
        var self = this;
        this.options = options;
        this.element = $(element);
        this.switches = [];
        //
        this.bind = this.element.data('bind');
        this.mode = this.element.data('mode');
        this.nulls = this.element.data('nulls');
        if (this.nulls) {
            this.nulls = this.nulls.split(',');
        }
        //
        this.element.children().each(function (index, el) {
            self._addSwitch(el);
        });

        $(this.options.scope.broadcaster).on(this.options.eventPrefix + this.bind, $.proxy(this, '_handleScopeChange'));
    };

    SwitchGroup.prototype = {
        _addSwitch: function (el) {
            if (typeof $(el).data('value') != 'undefined') {
                $(el).switchItem()
                    .on('switch:selected', $.proxy(this, '_handleSwitchSelected'));
                this.switches.push(el);
                var currentVal = this.options.scope.get(this.bind);
                if ($.isArray( currentVal )) {
                    if (_.indexOf(currentVal, $(el).data('value')) >= 0) {
                        $(el).switchItem('select');
                    }
                } else {
                    if ($(el).data('value') === currentVal) {
                        $(el).switchItem('select');
                    }
                }
            }
        },
        addElement: function (html) {
            this.element.append(html);
            this._addSwitch(this.element.children().last());
        },
        _handleSwitchSelected: function (e, data) {
            this.options.scope.set(this.bind, data.value);
            if (this.nulls) {
                for (var i=0, len=this.nulls.length; i<len; i++) {
                    this.options.scope.set(this.nulls[i],null);
                }
            }
        },
        _handleScopeChange: function (e, data) {
            if (data.value === null) {
                this.reset();
                return false;
            }
            if (this.mode === 'multi') {
                if (data.oldValue.length > data.value.length) { // something was removed from the list
                    for (var i=0; i<data.oldValue.length; i++) {
                        if (_.indexOf(data.value, data.oldValue[i]) === -1) {
                            this.element.find('[data-value="' + data.oldValue[i] + '"]').switchItem('deselect');
                        }
                    }
                } else { // something was added to the list
                    for (var i=0; i<data.value.length; i++) {
                        var $el = this.element.find('[data-value="' + data.value[i] + '"]');
                        if (!$el.switchItem('isSelected')) {
                            $el.switchItem('select');
                        }
                    }
                }
            } else {
                this.element.find('[data-value="' + data.oldValue + '"]').switchItem('deselect');
                this.element.find('[data-value="' + data.value + '"]').switchItem('select');
            }
        },
        reset: function () {
            for (var i=0; i<this.switches.length; i++) {
                $(this.switches[i]).switchItem('deselect');
            }
        }
    };
    $.widget.bridge('switchGroup', SwitchGroup);

})(jQuery, window, document, window._);