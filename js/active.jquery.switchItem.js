'use strict';

(function ($, window, document, undefined) {

//////////////////////////////////////////////////////////////////////////////
// $.fn.switchItem()
// jQuery plugin/widget
// single on/off stated switch - does not bind directly to scope
// instead broadcasts change events up the stream to be managed by searchFilterBar()
//////////////////////////////////////////////////////////////////////////////

    var SwitchItem = function (options, element) {
        this.options = options;
        this.element = $(element);
        this.selected = false;
        this.value = this.element.data('value');
        this.element.bind('click',$.proxy(this,'_handleClick'));
    };

    SwitchItem.prototype = {
        _handleClick: function (e) {
            this.element.trigger('switch:selected', { value: this.value });
        },
        getValue: function () {
            return this.value;
        },
        isSelected: function () {
            return this.selected;
        },
        select: function () {
            this.selected = true;
            this.element.addClass('active');
        },
        deselect: function () {
            this.selected = false;
            this.element.removeClass('active');
        }
    }; // SwitchItem.prototype

    $.widget.bridge('switchItem', SwitchItem);


})(jQuery, window, document);