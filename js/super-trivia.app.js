'use strict';

angular.module('trivix.super-trivia.app',[])
	.factory('authInterceptor', ['$rootScope','$q','$window','ACCESS_TOKEN', function ($rootScope, $q, $window, ACCESS_TOKEN) {
		return {
			request: function (config) {
				config.headers = config.headers || {};
				if (ACCESS_TOKEN) {
					config.headers.Authorization = 'Bearer ' + ACCESS_TOKEN;
				}
				return config;
			},
			response: function (response) {
				if (response.status === 403) {
				// handle the case where the user is not authenticated
				}
				return response || $q.when(response);
			}
		};
	}])
	.config(['$anchorScrollProvider','$httpProvider', function ($anchorScrollProvider, $httpProvider) {
		$httpProvider.interceptors.push('authInterceptor');
		$anchorScrollProvider.disableAutoScrolling();
	}])

	.service('SuperTriviaService', ['$http', '$q', 'API_URL', 'ACCESS_TOKEN', function ($http, $q, api_root, ACCESS_TOKEN) {

		return {
			startGame: function () {
				return $http.get(api_root + '/super-trivia/start');
			}
			,getNextQuestion: function (question_id) {
				return $http.get(api_root + '/super-trivia/next-question/' + question_id);
			}
			,submitAnswer: function (time_remaining, question_id, option_id) {
				var post = { score: time_remaining, answer_id: option_id };
				return $http.post(api_root + '/super-trivia/submit-answer/' + question_id, post);
			}
			,saveResults: function (play_obj) {
				if (ACCESS_TOKEN != false) {
					return $http.post(api_root + '/super-trivia/save', { play: play_obj });
				} else {
					return false;
				}
			}
			,flagQuestion: function (flag_obj, question_id) {
				flag_obj.question_id = question_id;
				return $http.post(api_root + '/question/flag', flag_obj);
			}
			,getTopScoresSnapshot: function () {
				return $http.get(api_root + '/super-trivia/leaders/true');
			}
		}
	}])

	.controller('SuperTriviaCtrl', ['$scope','$rootScope','$filter','SuperTriviaService','_', function ($scope, $rootScope, $filter, SuperTriviaService, _) {

		$scope.mode = 'intro'; // intro, loading, playing, ad, complete

		$scope.currentQuestion = {};
		$scope.currentQuestionIndex = -1;
		$scope.timeRemaining = $scope.totalTime = 30;
		$scope.timeRemainingMessage = "Time Remaining";
		$scope.countdownInterval;

		$scope.answerStatus = 'unanswered';
		$scope.lastAnswerIncorrect = false;

		$scope.factoid = false;

		$scope.gameComplete = false;

		$scope.play = {};

		$scope.loading = false;
		$scope.loadingQuestion = false;

		$scope.numMisses = 0;
		

		$scope.startGame = function () {
			$scope.mode = 'loading';
			$scope.currentQuestion = {};
			$scope.currentQuestionIndex = -1;
			$scope.gameComplete = false;
			SuperTriviaService.startGame()
				.success(function (game) {
					$scope.$evalAsync(function () {
						$scope.play = game;		
						$scope.play.incorrect = 0;				
						$scope.mode = 'playing';
						$scope.goNextQuestion();
					});					
				});
			$rootScope.$emit('updateLeaderboard');
		}

		function loadNextQuestion () {
			clearInterval($scope.countdownInterval);
			$scope.answerStatus = 'unanswered';
			$scope.lastAnswerIncorrect = false;
			$scope.loading = true;
			$scope.loadingQuestion = true;
			SuperTriviaService.getNextQuestion($scope.play.question_ids[$scope.currentQuestionIndex])
				.success(function (response) {
					$scope.mode = 'playing';
					$scope.currentQuestion = response;
					$scope.startCountdown();
					$scope.loading = false;
					$scope.loadingQuestion = false;
				});
		}

		$scope.skipAd = function () {
			$rootScope.$broadcast('hideInterstitial');
			$scope.goNextQuestion();
		}

		$scope.getCurrentQuestion = function () {
			return $scope.currentQuestion;
		}

		$scope.totalQuestions = function () { 
			if ($scope.play.question_ids) {
				return $scope.play.question_ids.length;
			} else {
				return 0;
			}
		}

		$scope.totalScore = function () {
			return $scope.play.score;
		}

		$scope.totalAnswered = function () {
			if ($scope.play.answer_history) {
				return $scope.play.answer_history.length;
			} else {
				return 0;
			}
		}

		$scope.questionsRemaining = function () {
			return $scope.totalQuestions() - $scope.totalAnswered();
		}

		$scope.startCountdown = function () {
			$scope.timeRemainingMessage = "Time Remaining";
			$scope.timeRemaining = $scope.totalTime;
			$scope.countdownInterval = setInterval(function(){
				$scope.$apply(function() {
					$scope.timeRemaining -= 1;
					if ($scope.timeRemaining == 0) {
						$scope.selectOption(null);	
					}
				});
			}, 1000);
			$rootScope.$broadcast('startCountdown');
		}

		$scope.goNextQuestion = function () {
			//console.log('$scope.goNextQuestion');
			$scope.flagged = false;
			$scope.showFlag = false;
			$scope.factoid = false;
			if ($scope.play.incorrect == 3) {
				$scope.handleGameComplete();
			} else {
				$scope.currentQuestionIndex += 1;
				loadNextQuestion();
			}
			/*
			if ((($scope.currentQuestionIndex == 3) || ($scope.currentQuestionIndex == 8)) && $scope.mode != 'ad') {
				$rootScope.$broadcast('showInterstitial');
				$scope.mode = 'ad';
			}
			*/
		}
		
		$scope.handleGameComplete = function () {
			$scope.gameComplete = true;
			$scope.mode = 'complete';
			SuperTriviaService.saveResults($scope.play);
		}	

		$scope.selectOption = function (option) {
			if ($scope.loading) return false;
			if ($scope.answerStatus != 'unanswered') return false;
			clearInterval($scope.countdownInterval);
			$rootScope.$broadcast('stopCountdown');
			$scope.answerStatus = 'pending';
			var cq = $scope.getCurrentQuestion();
			var cqid = cq.question_id;
			var oid;
			if (!option || option == null) {
				oid = 0;
			} else {
				oid = option.answer_id;
			}			
			$scope.loading = true;
			//$scope.loadingQuestion = false;
			SuperTriviaService.submitAnswer( $scope.timeRemaining, cqid, oid )
				.success(function (response) {
					$scope.answerStatus = 'answered';
					$scope.lastAnswerIncorrect = !response.correct;
					var history_obj = {};
					$scope.timeRemainingMessage = response.correct ? "You Scored" : 'Sorry, 0 points';
					history_obj.correct = response.correct;
					history_obj.score = 0;
					history_obj.answered_on = response.answered_on;
					if (response.correct == true) {
						history_obj.score = response.score;
						$scope.play.correct += 1;
					} else {
						$scope.play.incorrect += 1;
						$rootScope.$broadcast('answeredIncorrectly');
					}
					$scope.play.answer_history.push(history_obj);
					$scope.timeRemaining = response.score;
					
					_.forEach(cq.answers, function (option) {
						var c = "";
						if ((response.correct == true) && (option.answer_id == response.correct_answer)) {
							c = "correct";
						} else if ((response.correct == false) && (option.answer_id == response.selected_answer)) {
							c = "incorrect";
						} else if (option.answer_id == response.correct_answer) {
							c = "right";
						}
						option.answer_status = c;
					});

					$scope.factoid = response.factoid || false;
					$scope.play.score += response.score;

					$scope.loading = false;

					$rootScope.$emit('updateLeaderboard');

				});
		}	

		$scope.showFlag = false;
		$scope.flagged = false;
		$scope.flagQuestion = {
			reason: null,
			description: ''
		}	
		$scope.flagQuestionReasons = [{key:'incorrect', value:'Incorrect'},{key:'typo', value:'Typo'},{key:'misleading', value:'Misleading'},{key:'offensive', value:'Offensive'}];
		$scope.flag = function () {
			console.log($scope.flagQuestion);
			if ($scope.flagQuestion.reason == '' || $scope.flagQuestion.reason == null) {
				alert('Please select a reason for flagging this question');
			} else {
				console.log('saving');
				SuperTriviaService.flagQuestion($scope.flagQuestion,$scope.currentQuestion.question_id)
					.then(function () {
						$scope.showFlag = false;
						$scope.flagQuestion = {
							reason: null,
							description: ''
						}
						$scope.flagged = true;
					});
			}
		}
		$scope.showFlagDialogue = function () {
			$scope.flagQuestion = {
				reason: null,
				description: ''
			}
			$scope.showFlag = true;
		}

	}])

	.controller('LeaderboardCtrl', ['$scope','$rootScope','$window','SuperTriviaService', function ($scope, $rootScope, $window, SuperTriviaService) {
		$scope.scores = [];
		$scope.loading = false;
		
		function loadScores () {
			if ($scope.loading == true) return;
			$scope.loading = true;
			SuperTriviaService.getTopScoresSnapshot()
				.success(function (response) {
					if (response.length > 5) {
						response.splice(5);
					}
					$scope.scores = response;
					$scope.loading = false;
				});
		}
		loadScores();

		$rootScope.$on('updateLeaderboard', loadScores);

		$scope.goFullLeaderboard = function () {
			$window.top.location.href = '/super-trivia-leaderboard/';
		}
		
	}])

	.service("_", function() {
		_.filterWithProperty = function( collection, name, value ) {
			var result = _.filter(
				collection,
				function( item ) {
					return( item[ name ] === value );
				}
			);
			return( result );
		};			
		_.rejectWithProperty = function( collection, name, value ) {
			var result = _.filter(
				collection,
				function( item ) {
					return( item[ name ] !== value );
				}
			);
			return( result );
		};
		_.findWithProperty = function( collection, name, value ) {
			var result = _.find(
				collection,
				function( item ) {
					return( item[ name ] === value );
				}
			);
			return( result );
		};
		_.sortOnProperty = function( collection, name, direction ) {
			var indicator = ( ( direction.toLowerCase() === "asc" ) ? -1 : 1 );
			collection.sort(
				function( a, b ) {
					if ( a[ name ] < b[ name ] ) {
						return( indicator );
					} else if ( a[ name ] > b[ name ] ) {
						return( - indicator );
					}
					return( 0 );
				}
			);
			return( collection );
		};
		return( _ );
	})
	

	.directive('interstitial', function () {
		return {
			scope: {
				skipAd: '&'
			}
			,template: 
				'<div ng-show="!!adShowing" style="position: relative; height: 300px;">' + 
					'<p ng-click="skipAd()" style="text-align: center; margin: 25px 0 10px 0; cursor: pointer;">Skip this ad</p>' + 
					'<div style="position: relative;"><iframe src="about:blank" width="300" height="250" frameborder="0" scrolling="no" style="position: absolute; left: 50%; margin-left: -150px; display: block;"></iframe></div>' + 
				'</div>'
			,link: function (scope, el, attrs) {	
				scope.container = el;
				scope.adShowing = false;
			}
			,controller: function ($scope) {
				$scope.$on('showInterstitial', function () {
					$scope.adShowing = true;
					var bodyStyles = 'margin: 0; width: 300px; height: 250px;';
					var c = '<!DOCTYPE html>'
					+ '<html><head></head>'
					+ '<body style="' + bodyStyles + '"><script src="http://ib.adnxs.com/ttj?id=2739495" type="text/javascript"></script></body></html>';
					var d = $scope.container.find('iframe')[0].contentWindow.document;
					d.open('text/html', 'replace');
					d.write(c);
					d.close();					
				});
				$scope.$on('hideInterstitial', function () {
					$scope.adShowing = false;					
				});
			}
		}
	})
	
	.directive('timerBar', function () {
		return {
			controller: function ($scope) {
				$scope.$on('startCountdown', function () {
					$scope.progressBar.css('width','100%').css('backgroundColor','#8bc541').animate({ width: 0 }, 30000, 'linear');
				});
				$scope.$on('stopCountdown', function () {
					$scope.progressBar.stop();
				});
				$scope.$on('answeredIncorrectly', function () {
					$scope.progressBar.css('backgroundColor','red').stop();
				});
			},
			link: function (scope, el, attrs) {
				scope.progressBar = el.find('.timer-bar-inner');
				scope.progressBar.css('width','100%');
			}
		}
	})

	
	.directive('countdownCircle', function () {
	
		var paper, indicatorArc;
		
		function drawCircle () {
			paper.clear();
			var strokeArc = paper.path().attr({
				"stroke": "#fff",
				"stroke-width": 10,
				arc: [66, 66, 100, 100, 62]
			});
			var bgArc = paper.path().attr({
				"stroke": "#d6d6d6",
				"stroke-width": 8,
				arc: [66, 66, 100, 100, 62]
			});
			
			indicatorArc = paper.path().attr({
				"stroke": "#8bc541",
				"stroke-width": 8,
				arc: [66, 66, 100, 100, 62]
			});
			
			indicatorArc.animate({
				arc: [66, 66, 0, 100, 62]
				}, 30000, "linear", function(){
					// anim complete here
				});
		}
		
		return {
			restrict: 'A',
			controller: function ($scope, $attrs) {
				$scope.$on('startCountdown', function () {
					drawCircle();
				});
				$scope.$on('stopCountdown', function () {
					indicatorArc.stop();
				});
				$scope.$on('answeredIncorrectly', function () {
					indicatorArc.attr("stroke", "#ff0000");
					indicatorArc.attr("opacity",0);
				});
			},
			link: function (scope, element, attrs) {
				paper = window.Raphael(element.context.id, 146, 146);
				paper.customAttributes.arc = function (xloc, yloc, value, total, R) {
					var alpha = 360 / total * value,
						a = (90 - alpha) * Math.PI / 180,
						x = xloc + R * Math.cos(a),
						y = yloc - R * Math.sin(a),
						path;
					if (total == value) {
						path = [
							["M", xloc, yloc - R],
							["A", R, R, 0, 1, 1, xloc - 0.01, yloc - R]
						];
					} else {
						path = [
							["M", xloc, yloc - R],
							["A", R, R, 0, +(alpha > 180), 1, x, y]
						];
					}
					return {
						path: path
					};
				};
			}
		}
	})
	

	.filter('unhash', function () {
		return function (str, chars, hashmap) {
			if (str && chars && hashmap) {
				var sa = str.split("");
				var len = sa.length;
				var a = [];
				for (var i=0; i<len; i++) {		
					var char = sa[i];			
					var key = _.indexOf(hashmap,char);
					if (key >= 0) char = chars[key];
					a.push(char);
				}
				return a.join("");
			} else {
				return str;
			}
		}
	})

	.filter('forceDecimal', function () {
		return function (num) {
			if ((Math.round(num) == num) && (num != 0)) num = num + ".0";
			return num;
		}
	})

	.filter('secondsFormat', function () {
		return function (seconds) {
			var mins = parseInt( seconds / 60 ) % 60;
			var secs = Math.floor(seconds % 60);
			return (mins) + ":" + (secs < 10 ? "0" + secs : secs);
		}
	})

	.filter('range', function() {
		return function(input, total) {
			total = parseInt(total);
			for (var i=0; i<total; i++)	input.push(i);
			return input;
		};
	})

	;